const express = require('express');
const compression = require('compression');
// const morgan = require("morgan");
const scProxy = require('@sitecore-jss/sitecore-jss-proxy').default;
const config = require('./config');
const cacheMiddleware = require('./cacheMiddleware');

const app = express();
const port = process.env.PORT || 3000;

// enable gzip compression for appropriate file types
app.use(compression());

// app.use(morgan('combined'));

// turn off x-powered-by http header
app.settings['x-powered-by'] = false;

// Serve static app assets from local /dist folder
app.use(
  '/dist',
  express.static('dist', {
    fallthrough: false, // force 404 for unknown assets under /dist
  })
);

/**
 * Output caching, can be enabled,
 * Read about restrictions here: {@link https://jss.sitecore.com/docs/techniques/performance/caching}
 */
// server.use(cacheMiddleware());

app.use((req, res, next) => {
  // because this is a proxy, all headers are forwarded on to the Sitecore server
  // but, if we SSR we only understand how to decompress gzip and deflate. Some
  // modern browsers would send 'br' (brotli) as well, and if the Sitecore server
  // supported that (maybe via CDN) it would fail SSR as we can't decode the Brotli
  // response. So, we force the accept-encoding header to only include what we can understand.
  if (req.headers['accept-encoding']) {
    req.headers['accept-encoding'] = 'gzip, deflate';
  }
  console.log(`reqURL: ${req.protocol}://${req.hostname}${req.originalUrl}`);
  // console.log(`resURL: ${res.protocol}://${res.hostname}${res.originalUrl}`);
  next();
})

// For any other requests, we render app routes server-side and return them
app.use('*', scProxy(config.serverBundle.renderView, config, config.serverBundle.parseRouteUrl));

// app.listen(port, () => {
//   console.log(`server listening on port ${port}!`);
// });

config.httpModule.httpsM.createServer(
  config.ssloptions, app).listen(port, () => {
    console.log(`server listening on port ${port}!`);
  });