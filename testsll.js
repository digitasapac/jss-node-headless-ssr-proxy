var express = require('express')
var fs = require('fs')
var https = require('https')
var app = express()

app.get('/', function (req, res) {
  res.send('hello world')
})

https.createServer({
  key: fs.readFileSync('sslcertificate/server.key'),
  cert: fs.readFileSync('sslcertificate/server.crt')
  //ca: fs.readFileSync('sslcertificate/ca_bundle.crt')
}, app)
  .listen(3000, function () {
    console.log('Example app listening on port 3000! Go to https://localhost:3000/')
  })